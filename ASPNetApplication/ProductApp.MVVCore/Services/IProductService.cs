﻿using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Services
{
    public interface IProductService
    {
        List<Product> GetProducts();
        int AddProduct(Product product);
        Product? GetProductById(int id);
        Product? DeleteProductById(int id);
        int DeleteProduct(int id);
        
    }
}
