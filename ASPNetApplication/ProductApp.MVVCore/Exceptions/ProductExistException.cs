﻿namespace ProductApp.MVVCore.Exceptions
{
    public class ProductExistException : Exception
    {
        public ProductExistException()
        {

        }
        public ProductExistException(string message):base(message)
        {

        }
    }
}
